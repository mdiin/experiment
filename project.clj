(defproject experiment2 "1.0.0"
  :dependencies [[integrant "0.7.0"]
                 [ring "1.7.0"]
                 [metosin/reitit "0.2.3"]
                 [rum "0.11.2"]
                 [org.roman01la/citrus "3.2.0"]
                 [org.clojure/clojure "1.10.0-beta3"]
                 ]
  :source-paths ["src/cljc"])

